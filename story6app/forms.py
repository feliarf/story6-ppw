from django import forms
class formstatus(forms.Form):
	description = {
		'id' : 'description',
		'class' : 'form-control',
		'text-align' : 'center',
	}

	status3 = forms.CharField(label='Status', required = True, max_length = 300, widget = forms.TextInput(attrs = description))