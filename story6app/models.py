from django.db import models

class story6(models.Model):
	status = models.CharField(max_length = 300)

class Subscriber(models.Model):
	email = models.CharField(max_length = 32, unique= True)
	name = models.CharField(max_length = 32)
	password = models.CharField(max_length = 32)