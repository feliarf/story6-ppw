from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import input1
from .views import index
from .views import datadiri
from .models import story6
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
  
    # Create your tests here.
class Story6UnitTest(TestCase):
	def test_lab_5_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_lab5_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_story6(self):
		new_activity = story6.objects.create(status = 'test')
		counting_all_available_story6 = story6.objects.all().count()
		self.assertEqual(counting_all_available_story6, 1)

	def test_nama_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'form.html')

	def test_challenge6_using_index_func(self):
		found = resolve('/datadiri/')
		self.assertEqual(found.func, datadiri)

	def test_challenge_6_url_is_exist(self):
		response = Client().get('/datadiri/')
		self.assertEqual(response.status_code, 200)

	def test_nama_template2(self):
		response = Client().get('/datadiri/')
		self.assertTemplateUsed(response, 'datadiri.html')

class Story6FunctionalTest(TestCase):
	def setUp(self): 
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Story6FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Story6FunctionalTest, self).tearDown()

	def test_can_start_a_list_and_retrieve_it_later(self):
		selenium = self.selenium

		#open the link
		self.selenium.get('http://127.0.0.1:8000')
		
		#find the form element
		description = selenium.find_element_by_id('description')
		submit = selenium.find_element_by_id('submit')

		#fill the form
		description.send_keys('coba coba')

		#submit form
		submit.send_keys(Keys.RETURN)

		# cek input
		self.assertIn('coba coba', selenium.page_source)

	def test_layout(self):
		selenium = self.selenium
		self.selenium.get('http://127.0.0.1:8000')
		header_text = self.selenium.find_element_by_tag_name('h1').text
		self.assertIn('Hello! Apa Kabar?', header_text )

	def test_css(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000')
		css = selenium.find_element_by_tag_name('body')
		body = css.get_attribute('style')
		self.assertIn("background-image: linear-gradient(to right, rgb(213, 188, 241), rgb(249, 181, 212), pink);", body)

	def test_layout_title(self):
		selenium = self.selenium
		self.selenium.get('http://127.0.0.1:8000')
		self.assertIn('Story 6 Felia', selenium.title)

	def test_tag_p(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000')
		text = selenium.find_element_by_tag_name('p').text
		self.assertIn("Tambahkan Status kamu!", text)

	def test_css_profil(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/accordionTema')
		css = selenium.find_element_by_id('tema')
		p = css.get_attribute('style')
		self.assertIn('margin-right: 23vw; margin-top: 0.5vw', p)

	def test_css_book(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/story9')
		css = selenium.find_element_by_tag_name('body')
		body = css.get_attribute('style')
		self.assertIn("background-image: linear-gradient(to right, rgb(213, 188, 241), rgb(249, 181, 212), pink);", body)

	

      #hehe



