from django.shortcuts import render
from .models import story6
from .forms import formstatus
from django.http import HttpResponseRedirect,JsonResponse,HttpResponse
import requests
from .models import Subscriber
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers


# Create your views here.
response = {}
response['counter'] = 0
def index(request) :
	response['form1'] = formstatus  #ini masukin kelas yang ada di form.py
	response['status2'] = story6.objects.all() #ini masukin kelas yang ada di model
	html = 'form.html'
	return render(request, html, response)

def input1(request) :
	form = formstatus(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['status'] = request.POST['status3'] #ini variable di forms.py
		stat = story6(status=response['status'])
		stat.save()
		return HttpResponseRedirect('../')
	else:
		return HttpResponseRedirect('../')

def datadiri(request):
	return render(request,'datadiri.html')

def accordionTema(request):
	return render(request,'accordion&tema.html')

def buku(request):
	return render(request,'buku.html')

def bukuList(request):
	if request.user.is_authenticated:
		request.session['user'] = request.user.username
		request.session['email'] = request.user.email
		if 'counter' not in request.session:
			request.session['counter'] = 0
		response['counter'] = request.session['counter']
		
	else:
		if 'counter' not in request.session:
			request.session['counter'] = 0
		response['counter'] = request.session['counter']
	return render(request, "buku.html", response)


def bukujson(request):
	txt = request.GET.get('cari', 'quilting')
	url = "https://www.googleapis.com/books/v1/volumes?q=" + txt
	data = requests.get(url).json()
	return JsonResponse(data)

def register(request):
	return render(request, 'register.html', {})

def registerPage(request):
	if (request.method == "POST"):
		data = dict(request.POST.items())
		print(data['name'])
		subscriber = Subscriber(name = data['name'],email = data['email'],password = data['password'])
		try :
			subscriber.full_clean()
			subscriber.save()
		except Exception as e:
			e = [v[0] for k, v in e.message_dict.items()]
			return JsonResponse({"error":e}, status=400)
		return JsonResponse({"message":"Data berhasil disimpan"})
	else:
		return render(request, 'register.html', {})

def availableEmail(request):
	email = request.GET["email"] #'email' dipake di html url
	emails = Subscriber.objects.filter(email=email).all()
	return JsonResponse({"email":email, "available" : not bool(emails)})

def listSubscribers(request):
	allSubscriber = Subscriber.objects.all().values('name', 'email', 'password')
	subscriber2 = list(allSubscriber)
	return JsonResponse(subscriber2, safe=False)

def htmlSubscribers(request):
	return render(request,'subscribers.html')

def addCounter(request):
	if request.user.is_authenticated:
		request.session['counter'] = request.session['counter'] + 1
	else:
		pass
	return HttpResponse(request.session['counter'], content_type = 'application/json')

@csrf_exempt
def kurangCounter(request):
	if request.user.is_authenticated:
		request.session['counter'] = request.session['counter'] - 1
	return HttpResponse(request.session['counter'], content_type = 'application/json')

def logoutView(request):
	request.session.flush()
	print(dict(request.session))
	logout(request)
	return HttpResponseRedirect('/story9')
