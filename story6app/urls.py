from django.conf.urls import url
from django.urls import path, include
from .views import index
from .views import input1
from .views import datadiri
from .views import accordionTema
from .views import buku
from .views import bukujson
from .views import register
from .views import registerPage
from .views import availableEmail
from .views import listSubscribers
from .views import htmlSubscribers
from .views import logoutView
from .views import bukuList
from .views import addCounter
from .views import kurangCounter

urlpatterns = [
	path('',index, name = 'index'),
	path('input1/', input1, name = 'input1'),
	path('datadiri/', datadiri, name = 'datadiri'),
	path('accordionTema/', accordionTema, name = 'accordionTema'),
	path('story9/', bukuList , name = 'buku'),
	path('fileJson/', bukujson, name = 'bukujson'),
	path('register/', register , name = 'register'),
	path('registerPage/', registerPage , name = 'registerPage'),
	path('available/', availableEmail , name = 'available'),
	path('subscriber/', listSubscribers , name = 'subscriber'),
	path('dataSubscriber/', htmlSubscribers , name = 'dataSubscriber'),
	path('auth/', include('social_django.urls', namespace='social')),  # <- Here
	path('logout/', logoutView, name='logout'),
	path('bukuList/', bukuList, name='bukuList'),
	path('addCounter/', addCounter, name ='addCounter'),
	path('kurangCounter/', kurangCounter, name ='kurangCounter'),
]